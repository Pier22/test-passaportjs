# Google OAuth2.0 Example - Express.js, Passport.js, Node.js.

# PassaportJS-G.Auth Docs: http://www.passportjs.org/packages/passport-google-oauth2/

## Run Local Server on localhost:3000
---

Procedure: 

0) creare il file .env qualora non fosse presente
1) ricorda di cambiare i token GOOGLE_CLIENT_ID e GOOGLE_CLIENT_SECRET qualora il token venisse cambiato nel file .env
1.1) setta il google auth su google platform
1) npm install (scarica i n.modules e le dipendenze)
2) npm start (avvia il server)

---
## Test di sicurezza e integrazione passaportJS con G.auth20 per il progetto di unibookbar.
## il seguente utilizzo e solo per test nel progetto di sicurezza si è infine utilizzato il JWT.